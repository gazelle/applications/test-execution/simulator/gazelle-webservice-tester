/// IMPORT
//////////////
import net.ihe.gazelle.wstester.mockrecord.Message
import net.ihe.gazelle.wstester.mockrecord.MessageRecorder

import java.nio.charset.StandardCharsets

/////////////////////////////////////
/////////////////////////////////////
def simulatedActorKeyword = "UNKNOWN"
def domainKeyword = "ITI"
def transactionKeyword = "None"
def standardKeyword = "application/soap+xml"
def responder_ip = "SoapUI Mock"
def responseType = "RegistryResponse"
def requestType = "SubmitObjectsRequest"

/////////////////////////////////////
/////////////////////////////////////

def request = mockRequest.requestContent;
def response = mockResponse.responseContent;
def sender_ip = mockRequest.getHttpRequest().getRemoteAddr()

/////// CAN BE CHANGED ACCORDING TO THE PROPERTIES USED IN THE RESPONSE
/////////////////////////////////////////////////////////////////////
response = response.replace('${#MockService#uuid2}', context.mockService.getPropertyValue('uuid2'))
response = response.replace('${#MockService#uuid1}', context.mockService.getPropertyValue('uuid1'))
response = response.replace('${#MockService#soapInfo}', context.mockService.getPropertyValue('soapInfo'))
////////

byte[] byte_request = request.getBytes(StandardCharsets.UTF_8)
byte[] byte_response = response.getBytes(StandardCharsets.UTF_8)

MessageRecorder messageRecorder = new MessageRecorder("jdbc:postgresql://localhost:5432/gazelle-webservice-tester", "gazelle", "gazelle")
Message requestMessage = new Message(sender_ip, sender_ip, requestType, simulatedActorKeyword, byte_request)
Message responseMessage = new Message(responder_ip, responder_ip, responseType, simulatedActorKeyword, byte_response)

messageRecorder.record(standardKeyword, transactionKeyword, domainKeyword, simulatedActorKeyword, requestMessage, responseMessage)


