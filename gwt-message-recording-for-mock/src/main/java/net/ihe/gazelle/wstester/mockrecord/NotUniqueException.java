package net.ihe.gazelle.wstester.mockrecord;

public class NotUniqueException extends Exception {

    public NotUniqueException(String s) {
        super(s);
    }
}
