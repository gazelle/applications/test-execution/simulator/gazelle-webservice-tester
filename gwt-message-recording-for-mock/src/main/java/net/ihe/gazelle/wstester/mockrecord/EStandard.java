package net.ihe.gazelle.wstester.mockrecord;

/*
 This is a duplication of simulator Common net.ihe.gazelle.simulator.message.model.EStandard
 */
public enum EStandard {

    HL7V2,
    HL7V3,
    XDS,
    FHIR_XML,
    FHIR_JSON,
    SVS,
    HPD,
    OTHER
}
