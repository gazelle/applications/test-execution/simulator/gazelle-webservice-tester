package net.ihe.gazelle.wstester.mockrecord;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class DAOModel {

    private DBConnection connection;

    DAOModel(DBConnection connection) {
        this.connection = connection;
    }


    /**
     * Get id of the tf_domain
     *
     * @return integer domain_id
     * @throws java.sql.SQLException
     */
    int getOrCreateDomainId(String keyword) throws MessageException, SQLException {
        String sql = "SELECT id from tf_domain where keyword = ?;";
        int result = getSelectId(sql, keyword);
        if (result == 0) {
            String insert = "INSERT INTO tf_domain (id, description, keyword, name) VALUES (nextval('tf_domain_id_seq'), NULL, ?, ?) returning id;";
            return insertNewTf(insert, keyword);
        } else {
            return result;
        }
    }


    /**
     * Get id of the tf_transaction
     *
     * @return integer transaction_id
     * @throws java.sql.SQLException
     */
    int getOrCreateTransactionId(String keyword) throws MessageException, SQLException {
        String sql = "SELECT id from tf_transaction where keyword = ?;";
        int result = getSelectId(sql, keyword);
        if (result == 0) {
            String insert = "INSERT INTO tf_transaction (id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), NULL, ?, ?) returning id;";
            return insertNewTf(insert, keyword);
        } else {
            return result;
        }
    }


    /**
     * Get id of the tf_actor
     *
     * @return integer actor_id
     * @throws java.sql.SQLException
     */
    int getOrCreateActorId(String keyword) throws MessageException, SQLException {
        String sql = "SELECT id from tf_actor where keyword = ?;";
        int result = getSelectId(sql, keyword);
        if (result == 0) {
            String insert = "INSERT INTO tf_actor (id, description, keyword, name) VALUES (nextval('tf_actor_id_seq'), NULL, ?, ?) returning id;";
            return insertNewTf(insert, keyword);
        } else {
            return result;
        }
    }


    /**
     * Get id with select request
     *
     * @return integer select id
     * @throws java.sql.SQLException
     */
    int getSelectId(String sql, String keyword) throws MessageException, SQLException {

        try (PreparedStatement st = connection.getConnection().prepareStatement(sql)) {
            st.setString(1, keyword);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    if (rs.isLast()) {
                        return rs.getInt("id");
                    } else {
                        throw new MessageException("We have found several id with the same keyword '" + keyword + "'");
                    }
                }
                return 0;
            } catch (Exception e) {
                throw new MessageException("An error occurred while searching the keyword '" + keyword + "'");
            }
        }
    }


    /**
     * Insert new domain, transaction or actor
     *
     * @throws java.sql.SQLException
     */
    int insertNewTf(String sql, String keyword) throws MessageException, SQLException {
        int id = 0;
        try (PreparedStatement st = connection.getConnection().prepareStatement(sql)) {
            st.setString(1, keyword);
            st.setString(2, keyword);
            try (ResultSet resultSet = st.executeQuery()) {
                if (resultSet.next()) {
                    id = (resultSet.getInt("id"));
                }
                return id;

            } catch (Exception e) {
                throw new MessageException("An error occurred while adding the new keyword '" + keyword + "'");
            }
        }
    }


}
