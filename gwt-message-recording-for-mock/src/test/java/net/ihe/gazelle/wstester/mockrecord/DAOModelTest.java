package net.ihe.gazelle.wstester.mockrecord;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DAOModelTest {

    @Mock
    private DataSource ds;
    @Mock
    private Connection c;
    @Mock
    private PreparedStatement stmt;
    @Mock
    private ResultSet rs;

    private DBConnection mockedDbConnection;


    @Before
    public void setUp() throws SQLException {
        assertNotNull(ds);
        mockedDbConnection = mock(DBConnection.class);
        when(c.prepareStatement(any(String.class))).thenReturn(stmt);
        when(mockedDbConnection.getConnection()).thenReturn(c);
        when(ds.getConnection()).thenReturn(c);
        when(rs.next()).thenReturn(true);
        when(rs.isLast()).thenReturn(true);
        when(rs.getInt("id")).thenReturn(3);
        when(stmt.executeQuery()).thenReturn(rs);
    }


    @Test
    public void getOrCreateDomainIdTest() throws MessageException, SQLException {
        DAOModel daoModel = new DAOModel(mockedDbConnection);
        Assert.assertEquals(daoModel.getOrCreateDomainId("CH"), 3);
    }

    @Test
    public void getOrCreateActorIdTest() throws MessageException, SQLException {
        DAOModel daoModel = new DAOModel(mockedDbConnection);
        Assert.assertEquals(daoModel.getOrCreateActorId("Provider"), 3);
    }

    @Test
    public void getOrCreateTransactionIdTest() throws MessageException, SQLException {
        DAOModel daoModel = new DAOModel(mockedDbConnection);
        Assert.assertEquals(daoModel.getOrCreateTransactionId("ADR"), 3);
    }

    @Test
    public void getSelectIdTest() throws MessageException, SQLException {
        DAOModel daoModel = new DAOModel(mockedDbConnection);
        Assert.assertEquals(daoModel.getSelectId("MOCK SELECT REQUEST", "TEST"), 3);
    }

    @Test
    public void insertNewTfTest() throws MessageException, SQLException {
        DAOModel daoModel = new DAOModel(mockedDbConnection);
        Assert.assertEquals(daoModel.insertNewTf("MOCK INSERT REQUEST", "TEST"), 3);
    }

    @Test(expected = MessageException.class)
    public void getSelectIdErrorTest() throws SQLException, MessageException {
        when(rs.isLast()).thenReturn(false);
        DAOModel daoModel = new DAOModel(mockedDbConnection);
        daoModel.getSelectId("MOCK SELECT REQUEST", "TEST");
    }

}