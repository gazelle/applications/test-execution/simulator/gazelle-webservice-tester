INSERT INTO gwt_profile (id, xdstools_name, tm_name) VALUES (nextval('gwt_profile_id_seq'), 'xds', null);
INSERT INTO gwt_profile (id, xdstools_name, tm_name) VALUES (nextval('gwt_profile_id_seq'), 'mhd', null);
INSERT INTO gwt_profile (id, xdstools_name, tm_name) VALUES (nextval('gwt_profile_id_seq'), 'xca-i', null);
INSERT INTO gwt_profile (id, xdstools_name, tm_name) VALUES (nextval('gwt_profile_id_seq'), 'xds-i', null);
INSERT INTO gwt_profile (id, xdstools_name, tm_name) VALUES (nextval('gwt_profile_id_seq'), 'fhir-init', null);

INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'mu', null, 'Metadata Update');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'mpq', null, 'Multi Patient Query');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'od', null, 'On Demand');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'isr', null, 'Integrated Source Repository');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'xua', null, 'XUA');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'catfolder', null, 'CAT Folder');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'catlifecycle', null, 'CAT Lifecycle');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'ad', null, 'Affinity Domain');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'docreciprespndr', null, 'MHD Document Recipient/Responder');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'mhddocrecip', null, 'MHD Document Recipient');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'xdsonfhir', null, 'XDS on FHIR');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'docreplace', null, 'Document Replace');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'lifecycle', null, 'Life Cycle');
INSERT INTO gwt_option (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_option_id_seq'), 'folder', null, 'Folder');

INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'xdrsrc', null, 'XDR Document Source');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'reg', null, 'XDS Document Registry');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'rep', null, 'XDS Document Repository');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'odds', null, 'On Demand Document Source');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'rec', null, 'XDR Document Recipient');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'rg', null, 'XCA Responding Gateway');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'ig', null, 'XCA Initiating Gateway');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'cons', null, 'XDS Document Consumer');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'ids', null, 'XDS-I Imaging Document Source');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'rig', null, 'XCA-I Responding Imaging Gateway');
INSERT INTO gwt_actor_type (id, xdstools_name, tm_name, description) VALUES (nextval('gwt_actor_type_id_seq'), 'idc', null, 'XDS-I Imaging Document Consumer');



