package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.wstester.model.CustomProperty;
import net.ihe.gazelle.wstester.model.CustomPropertyUsed;
import net.ihe.gazelle.wstester.model.Execution;
import net.ihe.gazelle.wstester.model.GwtProject;
import net.ihe.gazelle.wstester.model.GwtTestCase;
import net.ihe.gazelle.wstester.model.GwtTestSuite;
import net.ihe.gazelle.wstester.model.SoapuiProject;
import net.ihe.gazelle.wstester.model.TestInstanceResult;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RunInstanceActionTest {

    @Test
    public void createProjectwithNullProject() throws Exception {
        RunInstanceAction runInstanceAction = new RunInstanceAction();
        SoapuiProject soapuiProject = new SoapuiProject();
        runInstanceAction.setCurrentProject(soapuiProject);
        Assert.assertNull(runInstanceAction.createWsdlProject());
    }

    @Test
    public void initTestInstanceResultOK() throws Exception {
        Execution oldExecution = new Execution();
        TestInstanceResult expected = new TestInstanceResult();
        expected.setLastStatus("OK");
        expected.setTestName("NAME");
        expected.setTestKeyword("KEYWORD");
        expected.setTestSuite(new GwtTestSuite());
        expected.setTestInstance(1);
        oldExecution.setTestInstanceResult(expected);

        RunInstanceAction runInstanceAction = new RunInstanceAction();

        TestInstanceResult testInstanceResult = runInstanceAction.initTestInstanceResultForReRun(oldExecution);
        Assert.assertEquals(expected.getTestSuite(), testInstanceResult.getTestSuite());
        Assert.assertEquals(expected.getTestInstance(), testInstanceResult.getTestInstance());
        Assert.assertEquals(expected.getTestName(), testInstanceResult.getTestName());
        Assert.assertEquals(expected.getLastStatus(), testInstanceResult.getLastStatus());
        Assert.assertEquals(expected.getTestKeyword(), testInstanceResult.getTestKeyword());
    }

    @Test
    public void retrieveTransactionOK() {
        RunInstanceAction runInstanceAction = new RunInstanceAction();

        TestInstanceResult testInstanceResult = new TestInstanceResult();
        GwtTestSuite gwtTestSuite = new GwtTestSuite();
        gwtTestSuite.setLabel("HP_gets_document_Patient_2of3");
        testInstanceResult.setTestSuite(gwtTestSuite);
        Execution execution = new Execution();
        execution.setTestInstanceResult(testInstanceResult);
        runInstanceAction.setExecution(execution);

        SoapuiProject soapuiProject = new SoapuiProject();
        soapuiProject.setXmlFilePath("src/test/resources/soapui_project.xml");
        runInstanceAction.setCurrentProject(soapuiProject);

        List<String> transactions = Arrays.asList("ITI-45", "CH:XUA Get X-User Assertion", "ITI-18", "ITI-43");

        Assert.assertEquals(transactions, runInstanceAction.retrieveTransaction());
    }

    /*@Test
    public void reRunTestInstanceOK() {
        Execution oldExecution = new Execution();
        TestInstanceResult expected = new TestInstanceResult();
        expected.setLastStatus("OK");
        expected.setTestName("NAME");
        expected.setTestKeyword("KEYWORD");
        GwtTestSuite gwtTestSuite = new GwtTestSuite();
        gwtTestSuite.setLabel("HP_gets_document_Patient_2of3");
        expected.setTestSuite(gwtTestSuite);
        expected.setTestInstance(1);
        oldExecution.setTestInstanceResult(expected);
        RunInstanceAction runInstanceAction = new RunInstanceAction();
        runInstanceAction.setExecution(oldExecution);

        oldExecution.setLaunchBy("USER");
        oldExecution.setCompany("COMPANY");
        oldExecution.setLaunchDate(new Date());
        oldExecution.setParametersUsed(new ArrayList<CustomPropertyUsed>());
        SoapuiProject soapuiProject = new SoapuiProject();
        soapuiProject.setXmlFilePath("src/test.tmp/resources/soapui_project.xml");
        runInstanceAction.setCurrentProject(soapuiProject);
        runInstanceAction.setTestInstanceResult(expected);
        runInstanceAction.startExecution(runInstanceAction.createProjectWithKeyStore("soapui.jks", "src/test.tmp/resources/"));

        Assert.assertEquals("OK", runInstanceAction.getExecution().getTestInstanceResult().getLastStatus());
    }*/

    //todo
    /*@Test
    public void getCustomPropertiesUsed(){
        CustomPropertyUsed customPropertyUsed;
        CustomProperty customProperty;
        List<CustomPropertyUsed> expected = new ArrayList<>();

        GwtProject gwtProject = new GwtProject();
        List<CustomPropertyUsed> customPropertyUsedByProject = new ArrayList<>();
        customPropertyUsed = new CustomPropertyUsed();
        customPropertyUsedByProject.add(customPropertyUsed);
        expected.add(customPropertyUsed);

        GwtTestSuite gwtTestSuite = new GwtTestSuite();
        List<CustomPropertyUsed> customPropertyUsedBySuite = new ArrayList<>();
        customPropertyUsed = new CustomPropertyUsed();
        customPropertyUsedBySuite.add(customPropertyUsed);
        List<GwtTestSuite> gwtTestSuites = new ArrayList<>();
        gwtTestSuites.add(gwtTestSuite);
        gwtProject.setTestSuites(gwtTestSuites);
        expected.add(customPropertyUsed);


        GwtTestCase gwtTestCase = new GwtTestCase();
        List<CustomPropertyUsed> customPropertyUsedByCase = new ArrayList<>();
        customPropertyUsed = new CustomPropertyUsed();
        customPropertyUsedByCase.add(customPropertyUsed);
        List<GwtTestCase> gwtTestCases = new ArrayList<>();
        gwtTestCases.add(gwtTestCase);
        gwtTestSuite.setTestCases(gwtTestCases);
        expected.add(customPropertyUsed);

        RunInstanceAction runInstanceAction = new RunInstanceAction();
        List<CustomPropertyUsed> result = runInstanceAction.getCustomPropertiesUsed(gwtProject);

        Assert.assertEquals(expected, result);


    }*/

}
