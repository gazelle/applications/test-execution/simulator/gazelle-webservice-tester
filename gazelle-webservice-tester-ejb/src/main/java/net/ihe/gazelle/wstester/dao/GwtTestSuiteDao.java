package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.wstester.model.GwtTestSuite;
import net.ihe.gazelle.wstester.model.GwtTestSuiteQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GwtTestSuiteDao {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestSuiteDao.class);

    public static GwtTestSuite getGwtTestSuite(String label, String project) {
        GwtTestSuiteQuery query = new GwtTestSuiteQuery();
        query.label().eq(label);
        query.gwtProject().soapuiProject().label().eq(project);
        return query.getUniqueResult();
    }
}
