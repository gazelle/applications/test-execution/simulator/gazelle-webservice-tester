package net.ihe.gazelle.wstester.xdstools.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.wstester.xdstools.model.ActorType;
import net.ihe.gazelle.wstester.xdstools.model.ActorTypeQuery;
import net.ihe.gazelle.wstester.xdstools.model.Option;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

@Name("actorTypeManager")
@Scope(ScopeType.PAGE)
public class ActorTypeManager {
    private FilterDataModel<ActorType> actorTypes;
    private Filter<ActorType> filter;
    boolean addActorType = false;
    ActorType newActorType = null;

    public FilterDataModel<ActorType> getActorTypes() {
        return new FilterDataModel<ActorType>(getFilter()) {
            @Override
            protected Object getId(ActorType actorType) {
                return actorType.getId();
            }
        };
    }

    public Filter<ActorType> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.filter = new Filter(this.getHqlCriterions(), params);
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        ActorTypeQuery query = new ActorTypeQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("tmName", query.tmName());
        criterion.addPath("xdstoolsName", query.xdstoolsName());
        criterion.addPath("description", query.description());
        return criterion;
    }

    public boolean isAddActorType() {
        return addActorType;
    }

    public void setAddActorType(boolean addActorType) {
        this.addActorType = addActorType;
    }

    public ActorType getNewActorType() {
        return newActorType;
    }

    public void setNewActorType(ActorType newActorType) {
        this.newActorType = newActorType;
    }

    public void merge() {
        merge(newActorType);
        cancel();
    }

    public void merge(ActorType actorTypeToMerge) {
        List<Option> optionList = Option.getAllOption();
        if (optionList != null) {
            for (Option option : optionList) {
                if (actorTypeToMerge.getXdstoolsName().equals(option.getXdstoolsName()) && actorTypeToMerge.getId().equals(option.getId())) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already an option with this OID");
                    return;
                }
            }
        }
        actorTypeToMerge.save();
    }

    public void cancel() {
        newActorType = null;
        addActorType = false;
    }

    public void addNewActorType() {
        newActorType = new ActorType();
        addActorType = true;
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }
}
