package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "gwt_cross_validation_result")
@SequenceGenerator(name = "gwt_cross_validation_result_sequence", sequenceName = "gwt_cross_validation_result_id_seq", allocationSize = 1)
public class XValidationResult implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_cross_validation_result_sequence")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "gwt_project_result_id")
    private GwtProjectResult gwtProjectResult;

    @Column(name = "log_oid")
    private String logOid;

    @Column(name = "xvalidation_date")
    private String xvalidationDate;

    @Column(name = "xvalidation_status")
    private String xvalidationStatus;

    @Column(name = "permanent_link")
    private String permanentLink;

    public XValidationResult() {
        super();
    }

    public XValidationResult(GwtProjectResult projectResult, String logOid) {
        this.gwtProjectResult = projectResult;
        this.logOid = logOid;
    }

    public XValidationResult save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        XValidationResult result = entityManager.merge(this);
        entityManager.flush();
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GwtProjectResult getGwtProjectResult() {
        return gwtProjectResult;
    }

    public void setGwtProjectResult(GwtProjectResult gwtProjectResult) {
        this.gwtProjectResult = gwtProjectResult;
    }

    public String getLogOid() {
        return logOid;
    }

    public void setLogOid(String logOid) {
        this.logOid = logOid;
    }

    public String getXvalidationDate() {
        return xvalidationDate;
    }

    public void setXvalidationDate(String xvalidationDate) {
        this.xvalidationDate = xvalidationDate;
    }

    public String getXvalidationStatus() {
        return xvalidationStatus;
    }

    public void setXvalidationStatus(String xvalidationStatus) {
        this.xvalidationStatus = xvalidationStatus;
    }

    public String getPermanentLink() {
        return permanentLink;
    }

    public void setPermanentLink(String permanentLink) {
        this.permanentLink = permanentLink;
    }
}

