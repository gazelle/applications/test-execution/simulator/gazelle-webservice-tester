package net.ihe.gazelle.wstester.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("testStep")
public class GwtTestStep extends TestComponent implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestStep.class);

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_case_id")
    private GwtTestCase testCase;

    @OneToMany(
            targetEntity = GwtTestStepResult.class,
            mappedBy = "testStep",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<GwtTestStepResult> gwtTestStepResults = new ArrayList<>();

    public GwtTestStep() {
        super();
    }

    @Override
    public String getClassName() {
        return GwtTestStep.class.getSimpleName();
    }

    public GwtTestStep(String label, List<CustomProperty> customProperties, GwtTestCase testCase, List<GwtTestStepResult> gwtTestStepResults) {
        super(label, customProperties);
        this.testCase = testCase;
        this.gwtTestStepResults = gwtTestStepResults;
    }

    public GwtTestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(GwtTestCase testCase) {
        this.testCase = testCase;
    }

    public List<GwtTestStepResult> getGwtTestStepResults() {
        return gwtTestStepResults;
    }

    public void setGwtTestStepResults(List<GwtTestStepResult> gwtTestStepResults) {
        this.gwtTestStepResults = gwtTestStepResults;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GwtTestStep testStep = (GwtTestStep) o;

        return testCase.equals(testStep.testCase);
    }

    @Override
    public int hashCode() {
        return testCase.hashCode();
    }

}
