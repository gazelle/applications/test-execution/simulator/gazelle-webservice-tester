package net.ihe.gazelle.wstester.bean;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.wstester.action.RunInstanceAction;
import net.ihe.gazelle.wstester.dao.SoapuiProjectDao;
import net.ihe.gazelle.wstester.model.Execution;
import net.ihe.gazelle.wstester.model.SoapuiProject;
import net.ihe.gazelle.wstester.model.TestComponent;
import net.ihe.gazelle.wstester.util.Tree;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.richfaces.event.TreeSelectionChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.event.AjaxBehaviorEvent;
import java.io.File;
import java.io.Serializable;
import java.util.List;

@Name("runSoapui")
@Scope(ScopeType.PAGE)
public class RunInstanceBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(RunInstanceBean.class);

    private boolean runInstance;

    private RunInstanceAction runInstanceAction;

    @Create
    public void init() {
        runInstance = false;
        runInstanceAction = new RunInstanceAction();
    }

    public boolean isRunInstance() {
        return runInstance;
    }

    public void setRunInstance(boolean runInstance) {
        this.runInstance = runInstance;
    }

    public RunInstanceAction getRunInstanceAction() {
        return runInstanceAction;
    }

    public void setRunInstanceAction(RunInstanceAction runInstanceAction) {
        this.runInstanceAction = runInstanceAction;
    }

    public SoapuiProject getCurrentProject() {
        return runInstanceAction.getCurrentProject();
    }

    public void setCurrentProject(SoapuiProject currentProject) {
        runInstanceAction.setCurrentProject(currentProject);
    }

    public TestComponent getComponentToExecute() {
        return runInstanceAction.getComponentToExecute();
    }

    public void setComponentToExecute(TestComponent componentToExecute) {
        runInstanceAction.setComponentToExecute(componentToExecute);
    }

    public Execution getExecution() {
        return runInstanceAction.getExecution();
    }

    public void setExecution(Execution execution) {
        runInstanceAction.setExecution(execution);
    }

    public int getTestInstanceId() {
        return runInstanceAction.getTestInstanceId();
    }

    public void setTestInstanceId(int testInstanceId) {
        runInstanceAction.setTestInstanceId(testInstanceId);
    }

    public List<SoapuiProject> getSoapuiProjects() {
        return SoapuiProjectDao.getAllSoapuiProject();
    }

    public List<SoapuiProject> getActiveSoapuiProjects() {
        return SoapuiProjectDao.getActiveSoapuiProjects();
    }

    @Transactional
    public GazelleTreeNodeImpl<TestComponent> getCurrentNodeAsTree() {
        return Tree.getComponentAsTree(getComponentToExecute(), true, true);
    }

    @Transactional
    public GazelleTreeNodeImpl<TestComponent> getProjectAsTree() {
        return Tree.getComponentAsTree(getCurrentProject().getGwtProject(), false, true);
    }

    public void reset() {
        runInstance = false;
        runInstanceAction = new RunInstanceAction();
        setExecution(null);
    }

    public void softReset(AjaxBehaviorEvent event) {
        runInstance = false;
        setComponentToExecute(null);
        setExecution(null);
    }

    public boolean isEhealthsuisse() {
        if (ApplicationConfiguration.getValueOfVariable("ehealthsuisse") != null) {
            return ApplicationConfiguration.getBooleanValue("ehealthsuisse");
        } else {
            return false;
        }
    }

    public void getAllInfo() {
        runInstanceAction.getAllInfo();
    }

    @Transactional
    public void setCurrentNodeEventAndPrepareExecution(AjaxBehaviorEvent event) {
        setComponentToExecute((TestComponent) event.getComponent().getAttributes().get("testComponent"));
        runInstanceAction.createExecutionWithCustomProperties();
    }

    @Transactional
    public void setCurrentNodeEventAndPrepareExecution2(TreeSelectionChangeEvent event) {
        if (event.getNewSelection().size() == 1) {
            setComponentToExecute((TestComponent) ((List) event.getNewSelection()).get(0));
            runInstanceAction.createExecutionWithCustomProperties();
        }
    }

    public String redirectToResult() {
        Execution execution = runInstanceAction.runTestInstance();
        return "/executionResult.seam?id=" + execution.getId();
    }

    public String redirectToResultTest() {
        Execution execution = runInstanceAction.run();
        return "/executionResult.seam?id=" + execution.getId();
    }

    public boolean isFileExist() {
        if (getCurrentProject() != null) {
            File file = new File(getCurrentProject().getXmlFilePath());
            return file.exists();
        }
        return false;
    }

}
