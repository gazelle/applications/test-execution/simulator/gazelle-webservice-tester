package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.evsclient.connector.api.EVSClientServletConnector;
import net.ihe.gazelle.evsclient.connector.model.EVSClientCrossValidatedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.wstester.model.GwtProjectResult;
import net.ihe.gazelle.wstester.model.XValidationResult;
import net.ihe.gazelle.wstester.model.XValidatorInputInfo;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public class GwtProjectXValidation extends GwtValidation {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestStepValidation.class);

    private List<EVSClientCrossValidatedObject> xValidatorInputInfoList;

    private GwtProjectResult gwtProjectResultSelected;

    private XValidationResult xvalidationResult;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////    Getters and setters     /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public List<EVSClientCrossValidatedObject> getxValidatorInputInfoList() {
        return xValidatorInputInfoList;
    }

    public void setxValidatorInputInfoList(List<EVSClientCrossValidatedObject> xValidatorInputInfoList) {
        this.xValidatorInputInfoList = xValidatorInputInfoList;
    }

    public GwtProjectResult getGwtProjectResultSelected() {
        return gwtProjectResultSelected;
    }

    public void setGwtProjectResultSelected(GwtProjectResult gwtProjectResultSelected) {
        this.gwtProjectResultSelected = gwtProjectResultSelected;
    }

    public XValidationResult getXvalidationResult() {
        return xvalidationResult;
    }

    public void setXvalidationResult(XValidationResult xvalidationResult) {
        this.xvalidationResult = xvalidationResult;
    }

    public GwtProjectXValidation() {
    }

    public GwtProjectXValidation(GwtProjectResult gwtProjectResultSelected, List<EVSClientCrossValidatedObject> xValidatorInputInfoList) {
        this.gwtProjectResultSelected = gwtProjectResultSelected;
        this.xValidatorInputInfoList = xValidatorInputInfoList;
    }

    public void init() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        if (urlParams != null && urlParams.size() > 0) {
            String externalId = urlParams.get("id");
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            if (externalId != null) {
                gwtProjectResultSelected = entityManager.find(GwtProjectResult.class, Integer.parseInt(externalId));
                if (gwtProjectResultSelected == null) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The given id does not match any test step result in the " +
                            "database");
                } else {

                    String xvalidationLogOid = EVSClientResults.getXValResultOidFromUrl(urlParams);
                    if (xvalidationLogOid != null) {
                        XValidationResult result = new XValidationResult(gwtProjectResultSelected, xvalidationLogOid);
                        gwtProjectResultSelected.setXvalidationResult(result);
                        getXValResultDetails(result);
                    }
                    gwtProjectResultSelected.save();

                    try {
                        String executionId = String.valueOf(gwtProjectResultSelected.getExecution().getId());
                        FacesContext.getCurrentInstance().getExternalContext().redirect("/gazelle-webservice-tester/executionResult.seam?id="
                                + executionId);

                    } catch (IOException e) {
                        LOG.error(e.getMessage());
                    }
                }
            }

        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The url used must have a problem, it is not possible to redirect to the " +
                    "execution test step.");
        }
    }

    public void xvalidate() throws IOException {

        String evsClientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String toolOid = ApplicationConfiguration.getValueOfVariable("tool_instance_oid");

        if (evsClientUrl == null) {
            LOG.error("evs_client_url is missing in app_configuration table");
        } else if (toolOid == null) {
            LOG.error("tool_instance_oid is missing in app_configuration table");
        } else {
            ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
            if (EVSClientResults.getXValidatorName(gwtProjectResultSelected.getXvalidatorOid(), evsClientUrl) != null || EVSClientResults.getXValidatorStatus(gwtProjectResultSelected.getXvalidatorOid(), evsClientUrl).equals("Available")) {
                EVSClientServletConnector.sendToXValidation(xValidatorInputInfoList, extContext, evsClientUrl, toolOid, gwtProjectResultSelected.getXvalidatorOid(), String.valueOf(gwtProjectResultSelected.getId()));
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The cross validator you want to call is not available or does not exist, therefore it can't be called.");
            }
        }
    }

    private void getXValResultDetails(final XValidationResult result) {
        String evsclientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String xvalidationDate = EVSClientResults.getXValidationDate(result.getLogOid(), evsclientUrl);
        result.setXvalidationDate(xvalidationDate);
        String xvalidationStatus = EVSClientResults.getXValidationStatus(result.getLogOid(), evsclientUrl);
        result.setXvalidationStatus(xvalidationStatus);
        String linkToResult = EVSClientResults.getXValidationPermanentLink(result.getLogOid(), evsclientUrl);
        result.setPermanentLink(linkToResult);
        xvalidationResult = result.save();
    }


    /*public Part[] createParts() {

        Part[] parts;

        parts = new Part[xValidatorInputInfoList.size()];
        if (xValidatorInputInfoList.size() != 0) {
            for (int index = 0; index < xValidatorInputInfoList.size(); index++) {
                //We give the keyword this time
                parts[index] = EVSClientServletConnector.addToParts(xValidatorInputInfoList.get(index), xValidatorInputInfoList.get(index).getInputName());
            }
            return parts;
        } else {
            return null;
        }
    }*/

}
