package net.ihe.gazelle.wstester.model;

import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "gwt_custom_property")
@SequenceGenerator(name = "gwt_custom_property_sequence", sequenceName = "gwt_custom_property_id_seq", allocationSize = 1)
public class CustomProperty implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_custom_property_sequence")
    private Integer id;

    @Column(name = "label", nullable = false)
    private String label;

    @Column(name = "default_value")
    @Lob
    @Type(type = "text")
    private String defaultValue;

    @Column(name = "to_modify")
    private Boolean toModify;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "test_component_id")
    private TestComponent testComponent;

    @OneToMany(
            fetch = FetchType.LAZY,
            targetEntity = CustomPropertyUsed.class,
            mappedBy = "customProperty",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CustomPropertyUsed> customPropertyUsedList = new ArrayList<>();

    public CustomProperty() {
        super();
    }

    public CustomProperty(TestComponent testComponent) {
        this.testComponent = testComponent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestComponent getTestComponent() {
        return testComponent;
    }

    public void setTestComponent(TestComponent testComponent) {
        this.testComponent = testComponent;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isToModify() {
        return toModify;
    }

    public void setToModify(boolean toModify) {
        this.toModify = toModify;
    }

    public List<CustomPropertyUsed> getCustomPropertyUsedList() {
        return customPropertyUsedList;
    }

    public void setCustomPropertyUsedList(List<CustomPropertyUsed> customPropertyUsedList) {
        this.customPropertyUsedList = customPropertyUsedList;
    }

    public CustomPropertyUsed getCustomPropertyUsedByExecution(Execution execution) {
        for (CustomPropertyUsed customPropertyUsed : customPropertyUsedList) {
            if (customPropertyUsed.getExecution().equals(execution)) {
                return customPropertyUsed;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomProperty customProperty = (CustomProperty) o;

        return testComponent.equals(customProperty.testComponent);
    }

    @Override
    public int hashCode() {
        return testComponent.hashCode();
    }
}

