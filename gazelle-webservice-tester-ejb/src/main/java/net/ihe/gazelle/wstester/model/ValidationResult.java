package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.wstester.dao.ExecutionDao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "gwt_validation_result")
@SequenceGenerator(name = "gwt_validation_result_sequence", sequenceName = "gwt_validation_result_id_seq", allocationSize = 1)
public class ValidationResult implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_validation_result_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "test_step_result_id")
    private GwtTestStepResult gwtTestStepResult;

    @Column(name = "oid")
    private String oid;

    @Column(name = "type")
    private String type;

    @Column(name = "validation_date")
    private String validationDate;

    @Column(name = "validation_status")
    private String validationStatus;

    @Column(name = "permanent_link")
    private String permanentLink;

    public ValidationResult() {
        super();
    }

    public ValidationResult(GwtTestStepResult selectedMessage, String oid, String type) {
        this.gwtTestStepResult = selectedMessage;
        this.oid = oid;
        this.type = type;
    }

    public ValidationResult save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        ValidationResult result = entityManager.merge(this);
        entityManager.flush();
        ExecutionDao.mergeExecution(gwtTestStepResult.getExecution());
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GwtTestStepResult getGwtTestStepResult() {
        return gwtTestStepResult;
    }

    public void setGwtTestStepResult(GwtTestStepResult gwtTestStepResult) {
        this.gwtTestStepResult = gwtTestStepResult;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getPermanentLink() {
        return permanentLink;
    }

    public void setPermanentLink(String permanentLink) {
        this.permanentLink = permanentLink;
    }

    public boolean isRequest() {
        if (type.equals("request")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isResponse() {
        if (type.equals("response")) {
            return true;
        } else {
            return false;
        }
    }
}
