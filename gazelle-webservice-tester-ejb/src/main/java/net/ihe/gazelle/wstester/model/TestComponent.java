package net.ihe.gazelle.wstester.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "component_type",
        discriminatorType = DiscriminatorType.STRING
)
@DiscriminatorValue("testComponent")
@Table(name = "gwt_test_component")
@SequenceGenerator(name = "gwt_test_component_sequence", sequenceName = "gwt_test_component_id_seq", allocationSize = 1)
public abstract class TestComponent implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_test_component_sequence")
    private Integer id;

    @Column(name = "label", nullable = false)
    private String label;

    @Column(name = "executable", nullable = false)
    private boolean executable;

    @Column(name = "disable")
    private boolean disable;

    @OneToMany(
            targetEntity = CustomProperty.class,
            fetch = FetchType.LAZY,
            mappedBy = "testComponent",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CustomProperty> customProperties = new ArrayList<>();

    @OneToMany(
            targetEntity = Execution.class,
            fetch = FetchType.LAZY,
            mappedBy = "testComponent",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Execution> executionList = new ArrayList<>();

    @Transient
    private boolean expendedInTree;

    public TestComponent() {
        super();
    }

    public TestComponent(String label, List<CustomProperty> customProperties) {
        this.label = label;
        this.customProperties = customProperties;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Execution> getExecutionList() {
        return executionList;
    }

    public void setExecutionList(List<Execution> executionList) {
        this.executionList = executionList;
    }

    public List<CustomProperty> getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(List<CustomProperty> customProperties) {
        this.customProperties = customProperties;
    }

    public List<CustomProperty> getCustomPropertiesToModify() {
        List<CustomProperty> customProperties = new ArrayList<>();
        for (CustomProperty customProperty : getCustomProperties()) {
            if (customProperty.isToModify()) {
                customProperties.add(customProperty);
            }
        }
        return customProperties;
    }

    public List<CustomPropertyUsed> getCustomPropertiesUsed(Execution execution) {
        List<CustomPropertyUsed> customProperties = new ArrayList<>();
        for (CustomProperty customProperty : getCustomProperties()) {
            if (customProperty.getCustomPropertyUsedByExecution(execution) != null) {
                customProperties.add(customProperty.getCustomPropertyUsedByExecution(execution));
            } else {
                CustomPropertyUsed customPropertyUsed = new CustomPropertyUsed(customProperty.getDefaultValue(), execution, customProperty);
                customProperties.add(customPropertyUsed);
            }
        }
        return customProperties;
    }

    public boolean isExecutable() {
        return executable;
    }

    public void setExecutable(boolean executable) {
        this.executable = executable;
    }

    public boolean isExpendedInTree() {
        return expendedInTree;
    }

    public void setExpendedInTree(boolean expendedInTree) {
        this.expendedInTree = expendedInTree;
    }

    public abstract String getClassName();

    public int numberOfCustumProperties() {
        if (customProperties != null) {
            return customProperties.size();
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestComponent that = (TestComponent) o;

        if (executable != that.executable) return false;
        if (disable != that.disable) return false;
        if (!id.equals(that.id)) return false;
        return label.equals(that.label);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + label.hashCode();
        result = 31 * result + (executable ? 1 : 0);
        result = 31 * result + (disable ? 1 : 0);
        return result;
    }
}
