package net.ihe.gazelle.wstester.xdstools.ws;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ws.client.GazelleTRMServiceClient;
import net.ihe.gazelle.ws.client.GazelleTRMServiceStub;
import net.ihe.gazelle.ws.client.SOAPExceptionException;
import net.ihe.gazelle.wstester.xdstools.model.ActorType;
import net.ihe.gazelle.wstester.xdstools.model.CallingTool;
import net.ihe.gazelle.wstester.xdstools.model.Nonce;
import net.ihe.gazelle.wstester.xdstools.model.Option;
import net.ihe.gazelle.wstester.xdstools.model.Profile;
import net.ihe.gazelle.wstester.xdstools.model.TestInstanceTm;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.List;


@Stateless
@Name("Xdstools")
@Path("/xdstools")
public class Xdstools {

    private static final Logger LOG = LoggerFactory.getLogger(Xdstools.class);

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/")
    public String getNonce(
            @QueryParam("testInstanceId") int testInstanceId,
            @QueryParam("callingToolOid") String callingToolOid) {
        CallingTool callingTool = CallingTool.getToolByOid(callingToolOid);
        if (callingTool == null) {
            return "Error : there is no calling tool with this OID";
        }

        GazelleTRMServiceClient gazelleTRMServiceClient;
        Integer testingSession = null;
        String systemKeyword = null;
        String actor = null;
        String integrationProfile = null;
        String integrationProfileOption = null;
        String actorCode = null;
        String roleInTest = null;
        try {
            gazelleTRMServiceClient = new GazelleTRMServiceClient(callingTool.getUrl() + "/gazelle-tm-ejb/GazelleTRMService/gazelleTRM");
            GazelleTRMServiceStub.TrmTestInstance trmTestInstance = gazelleTRMServiceClient.getTestInstanceById(testInstanceId);
            testingSession = trmTestInstance.getTestingSessionId();
            if (testingSession == null) {
                return "Error : there is no testing session in the test instance";
            }

            GazelleTRMServiceStub.TrmParticipant[] trmParticipantTab = trmTestInstance.getParticipants();
            for (GazelleTRMServiceStub.TrmParticipant trmParticipant : trmParticipantTab) {
                if (!gazelleTRMServiceClient.getSystemIsATool(trmParticipant.getSystem(), testingSession)) {
                    systemKeyword = trmParticipant.getSystem();
                    actor = trmParticipant.getAipo().getActor();
                    integrationProfile = trmParticipant.getAipo().getIntegrationProfile();
                    integrationProfileOption = trmParticipant.getAipo().getIntegrationProfileOption();
                    int roleInTestId = trmParticipant.getRoleInTestId();
                    for (GazelleTRMServiceStub.TrmTestRole trmTestRole : trmTestInstance.getTest().getTrmTestRoles()) {
                        if (roleInTestId == trmTestRole.getRoleInTest().getId()) {
                            roleInTest = trmTestRole.getRoleInTest().getKeyword();
                            break;
                        }
                    }

                }
            }
            if (systemKeyword == null) {
                return "Error : there is no system which is a tool in the test instance";
            }
        } catch (SOAPExceptionException e) {
            LOG.error(e.getMessage());
        } catch (RemoteException e) {
            LOG.error(e.getMessage());
        }

        if (testingSession == null || systemKeyword == null) {
            return "Error : Could not retrieve the testing session or the system keyword from Gazelle TM";
        }

        List<TestInstanceTm> testInstanceTmList = TestInstanceTm.getTestInstance(testingSession, systemKeyword, callingTool);

        String actorType = ActorType.getActorTypeByTmName(roleInTest);
        String profile = Profile.getProfileByTmName(integrationProfile);
        String option = Option.getOptionByTmName(integrationProfileOption);

        if (testInstanceTmList != null) {
            boolean testInstanceIdPresent = false;
            for (TestInstanceTm testInstanceTm : testInstanceTmList) {
                if (testInstanceTm.getTestInstanceId() == testInstanceId) {
                    testInstanceIdPresent = true;
                }
            }
            if (!testInstanceIdPresent) {
                TestInstanceTm newTestInstanceTm = new TestInstanceTm(testInstanceId, systemKeyword, testingSession, testInstanceTmList.get(0)
                        .getNonce(), callingTool);
                newTestInstanceTm.merge();
            }
            String redirectUrl = testInstanceTmList.get(0).getNonce().getUrl() + actorType + "/" + profile + "/" + option + "/" + systemKeyword;
            return redirect(redirectUrl);
        } else {
            if (PreferenceService.getString("xdstools_url") == null) {
                return "Error : preference \"xdstools_url\" is not set in Gazelle Webservice Tester";
            } else {
                String url = PreferenceService.getString("xdstools_url") + "CasSessionBuilder?testingSessionId=" + testingSession + ";systemName="
                        + systemKeyword;
                String response = getHTML(url);
                if (response != null && !response.isEmpty()) {
                    String baseUrl = PreferenceService.getString("xdstools_url") + "Xdstools2.html#ConfActor:default/" + response + "/";
                    String redirectUrl = baseUrl + actorType + "/" + profile + "/" + option + "/" + systemKeyword;
                    Nonce nonce = new Nonce(response, baseUrl);
                    TestInstanceTm newTestInstanceTm = new TestInstanceTm(testInstanceId, systemKeyword, testingSession, nonce, callingTool);
                    nonce.addTestInstanceTm(newTestInstanceTm);
                    nonce.merge();
                    return redirect(redirectUrl);
                } else {
                    return "Error : Could not retrieve the nonce from xdstools (" + url + ")";
                }
            }
        }
    }

    public String getHTML(String urlToRead) {
        StringBuilder result = new StringBuilder();
        URL url;
        BufferedReader rd = null;
        HttpURLConnection conn = null;
        try {
            urlToRead = urlToRead.replaceAll(" ", "%20");
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result.toString();
    }

    public String redirect(String url) {
        StringBuilder redirect = new StringBuilder();
        redirect.append("<!doctype html>\n");
        redirect.append("<html>\n");
        redirect.append("<head>\n");
        redirect.append("<meta charset=\"utf-8\">\n");
        redirect.append("<meta http-equiv=\"refresh\" content=\"0; url=" + url + "/\" />\n");
        redirect.append("</head>\n");
        redirect.append("<body>\n");
        redirect.append("<p><a href=\"" + url + "\">Redirect</a></p>\n");
        redirect.append("</body>\n");
        redirect.append("</html>\n");
        return redirect.toString();
    }
}
