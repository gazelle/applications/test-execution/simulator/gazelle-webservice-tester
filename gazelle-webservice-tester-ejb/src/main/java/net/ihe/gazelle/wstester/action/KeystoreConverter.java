package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.wstester.dao.KeystoreDao;
import net.ihe.gazelle.wstester.model.Keystore;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Name("keystoreConverter")
@BypassInterceptors
@Converter(forClass = Keystore.class)
public class KeystoreConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return KeystoreDao.getKeystoreByLabel(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o instanceof Keystore) {
            Keystore keystore = (Keystore) o;
            return keystore.getLabel();
        } else {
            return null;
        }
    }
}
