package net.ihe.gazelle.wstester.model;

import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;
import net.ihe.gazelle.wstester.util.DownloadFile;
import net.ihe.gazelle.wstester.util.Xml;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

@Entity
@Table(name = "gwt_attachement")
@SequenceGenerator(name = "gwt_attachement_sequence", sequenceName = "gwt_attachement_id_seq", allocationSize = 1)
public class GwtAttachment implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestStepResult.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_attachement_sequence")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "size")
    private long size;

    @Column(name = "part")
    private String part;

    @Column(name = "content")
    private byte[] content;

    @Column(name = "url")
    private String url;

    @Column(name = "content_id")
    private String contentId;

    @Column(name = "encoding")
    private String encoding;

    @Transient
    private Boolean indentFile;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public Boolean getIndentFile() {
        return indentFile;
    }

    public void setIndentFile(Boolean indentFile) {
        this.indentFile = indentFile;
    }

    public String getContentFormatted() {
        try {
            if (content != null) {
                if (contentType.equals(MediaType.TEXT_HTML) || contentType.equals(MediaType.TEXT_PLAIN) || contentType.equals(MediaType.TEXT_XML)) {
                    return XhtmlRendererFactory.getRenderer("xml").highlight(null, Xml.prettyFormat(new String(content,
                            StandardCharsets.UTF_8)
                    ), "UTF-8", false).replaceAll("<h1>.*</h1>", "");
                }
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    public void downloadAttachement() {
        if (content != null) {
            final String content = new String(this.content);
            DownloadFile.downloadMessage(content, name);
        }
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }
}
