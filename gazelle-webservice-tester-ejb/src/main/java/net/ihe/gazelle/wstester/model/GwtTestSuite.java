package net.ihe.gazelle.wstester.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("testSuite")
public class GwtTestSuite extends TestComponent implements Serializable {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    private GwtProject gwtProject;

    @OneToMany(
            fetch = FetchType.LAZY,
            targetEntity = GwtTestCase.class,
            mappedBy = "testSuite",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Fetch(value = FetchMode.SUBSELECT)
    private List<GwtTestCase> testCases;

    @OneToMany(
            targetEntity = TestInstanceResult.class,
            mappedBy = "testSuite",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<TestInstanceResult> testInstanceResults = new ArrayList<>();

    public GwtTestSuite() {
        super();
    }

    public GwtTestSuite(GwtProject project, List<GwtTestCase> testCases, String label, List<CustomProperty> customProperties) {
        super(label, customProperties);
        this.gwtProject = project;
        this.testCases = testCases;
    }

    public GwtProject getProject() {
        return gwtProject;
    }

    public void setProject(GwtProject project) {
        this.gwtProject = project;
    }

    public List<GwtTestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<GwtTestCase> testCases) {
        this.testCases = testCases;
    }

    public List<TestInstanceResult> getTestInstanceResults() {
        return testInstanceResults;
    }

    public void setTestInstanceResults(List<TestInstanceResult> testInstanceResults) {
        this.testInstanceResults = testInstanceResults;
    }

    public List<GwtTestCase> getTestCasesWithCustomPropertiesToModify() {
        List<GwtTestCase> gwtTestCases = new ArrayList<>();
        for (GwtTestCase gwtTestCase : getTestCases()) {
            for (CustomProperty customProperty : gwtTestCase.getCustomProperties()) {
                if (customProperty.isToModify()) {
                    gwtTestCases.add(gwtTestCase);
                    break;
                }
            }
        }
        return gwtTestCases;
    }

    @Override
    public String getClassName() {
        return GwtTestSuite.class.getSimpleName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GwtTestSuite testSuite = (GwtTestSuite) o;

        if (!gwtProject.equals(testSuite.gwtProject)) {
            return false;
        }
        return testCases.equals(testSuite.testCases);
    }

    @Override
    public int hashCode() {
        int result = gwtProject.hashCode();
        result = 31 * result + testCases.hashCode();
        return result;
    }
}
