package net.ihe.gazelle.wstester.xdstools.model;

import org.jboss.seam.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "gwt_actor_type")
@SequenceGenerator(name = "gwt_actor_type_sequence", sequenceName = "gwt_actor_type_id_seq", allocationSize = 1)
public class ActorType implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_actor_type_sequence")
    private Integer id;

    @Column(name = "xdstools_name", nullable = false)
    private String xdstoolsName;

    @Column(name = "tm_name")
    private String tmName;

    @Column(name = "description")
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXdstoolsName() {
        return xdstoolsName;
    }

    public void setXdstoolsName(String xdstoolsName) {
        this.xdstoolsName = xdstoolsName;
    }

    public String getTmName() {
        return tmName;
    }

    public void setTmName(String tmName) {
        this.tmName = tmName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<ActorType> getAllActorType() {
        final ActorTypeQuery query = new ActorTypeQuery();
        return query.getListNullIfEmpty();
    }

    public static String getActorTypeByTmName(String tmName) {
        final ActorTypeQuery query = new ActorTypeQuery();
        List<ActorType> actorTypes = query.getListNullIfEmpty();
        for (ActorType actorType : actorTypes) {
            if (actorType.getTmName() != null) {
                for (String actor : actorType.getTmName().split(",")) {
                    if (actor.trim().equals(tmName)) {
                        return actorType.getXdstoolsName();
                    }
                }
            }
        }
        return null;
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ActorType actorType = (ActorType) o;

        if (!xdstoolsName.equals(actorType.xdstoolsName)) {
            return false;
        }
        return tmName != null ? tmName.equals(actorType.tmName) : actorType.tmName == null;
    }

    @Override
    public int hashCode() {
        int result = xdstoolsName.hashCode();
        result = 31 * result + (tmName != null ? tmName.hashCode() : 0);
        return result;
    }
}
