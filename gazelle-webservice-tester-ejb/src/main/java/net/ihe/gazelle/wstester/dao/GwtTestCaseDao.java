package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.wstester.model.GwtTestCase;
import net.ihe.gazelle.wstester.model.GwtTestCaseQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GwtTestCaseDao {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestCaseDao.class);

    public static GwtTestCase getGwtTestCase(String labelTestCase, String labelTestSuite, String project) {
        GwtTestCaseQuery query = new GwtTestCaseQuery();
        query.label().eq(labelTestCase);
        query.testSuite().label().eq(labelTestSuite);
        query.testSuite().gwtProject().label().eq(project);
        return query.getUniqueResult();
    }
}
