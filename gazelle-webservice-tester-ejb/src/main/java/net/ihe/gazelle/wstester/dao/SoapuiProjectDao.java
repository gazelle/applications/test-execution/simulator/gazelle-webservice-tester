package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.wstester.model.SoapuiProject;
import net.ihe.gazelle.wstester.model.SoapuiProjectQuery;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class SoapuiProjectDao {


    private static final Logger LOGGER = LoggerFactory.getLogger(SoapuiProjectDao.class);

    public static SoapuiProject mergeSoapuiProject(SoapuiProject soapuiProject) {
        if (soapuiProject.getXmlFilePath() == null || soapuiProject.getXmlFilePath().isEmpty()) {
            SoapuiProjectDao.LOGGER.error("Cannot merge the given SoapuiProject because XmlFilePath is null");
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The path of the SoapUI Project is empty");
            return null;
        } else {
            final File uploadedFile = new File(soapuiProject.getXmlFilePath());
            if (!uploadedFile.exists()) {
                SoapuiProjectDao.LOGGER.error("The given file does not exist: {}", soapuiProject.getXmlFilePath());
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The SoapUI Project file does not exist");
                return null;
            }
            final EntityManager em = EntityManagerService.provideEntityManager();
            SoapuiProject newSoapuiProject = em.merge(soapuiProject);
            em.flush();
            return newSoapuiProject;
        }
    }


    public static SoapuiProject getSoapuiProjectById(Integer id) {
        return EntityManagerService.provideEntityManager().find(SoapuiProject.class, id);
    }

    public static SoapuiProject getSoapuiProjectByLabel(String label) {
        SoapuiProjectQuery query = new SoapuiProjectQuery();
        query.label().eq(label);
        return query.getUniqueResult();
    }

    public static List<SoapuiProject> getAllSoapuiProject() {
        SoapuiProjectQuery query = new SoapuiProjectQuery();
        query.getList();
        return query.getListNullIfEmpty();
    }

    public static List<SoapuiProject> getActiveSoapuiProjects() {
        SoapuiProjectQuery query = new SoapuiProjectQuery();
        query.active().eq(true);
        List<SoapuiProject> listNullIfEmpty = query.getListNullIfEmpty();

        if (listNullIfEmpty != null) {
            Collections.sort(listNullIfEmpty, new Comparator<SoapuiProject>() {
                @Override
                public int compare(SoapuiProject soapuiProject, SoapuiProject soapuiProject2) {

                    return soapuiProject.getLabel().compareToIgnoreCase(soapuiProject2.getLabel());
                }
            });
        }

        return listNullIfEmpty;
    }
}
