package net.ihe.gazelle.wstester.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "test_step_info")
@SequenceGenerator(name = "test_step_info_sequence", sequenceName = "test_step_info_id_seq", allocationSize = 1)
public class TestStepInfo implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_step_info_sequence")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gwt_project_result_id")
    private GwtProjectResult gwtProjectResult;

    @Column(name = "test_step_name")
    private String testStepName;

    @Column(name = "input_type")
    private String inputType;

    public TestStepInfo() {super();}

    public TestStepInfo(GwtProjectResult projectResult, String testStepName, String inputType) {
        this.gwtProjectResult = projectResult;
        this.testStepName = testStepName;
        this.inputType = inputType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GwtProjectResult getGwtProjectResult() {
        return gwtProjectResult;
    }

    public void setGwtProjectResult(GwtProjectResult gwtProjectResult) {
        this.gwtProjectResult = gwtProjectResult;
    }

    public String getTestStepName() {
        return testStepName;
    }

    public void setTestStepName(String testStepName) {
        this.testStepName = testStepName;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

}
