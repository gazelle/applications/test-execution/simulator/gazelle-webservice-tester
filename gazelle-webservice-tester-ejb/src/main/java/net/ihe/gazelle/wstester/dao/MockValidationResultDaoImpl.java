package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.wstester.model.MockValidationResult;
import net.ihe.gazelle.wstester.model.MockValidationResultQuery;
import org.jboss.seam.annotations.Name;

import java.util.List;

@Name("mockValidationResultDAO")
public class MockValidationResultDaoImpl implements MockValidationResultDao {

    /**
     * Retrieve last MockValidationResult using Messageinstance
     *
     * @param messageInstance Message instance used to retrieve MockValidationResult
     *
     * @return MockValidationResult Corresponding last MockValidationResult
     */
    public MockValidationResult getLastMockValidationResultByMessageInstance(MessageInstance messageInstance) {
        MockValidationResultQuery query = new MockValidationResultQuery();
        query.messageInstance().id().eq(messageInstance.getId());
        List<MockValidationResult> mockValidationResultList = query.getList();
        if (mockValidationResultList != null && !mockValidationResultList.isEmpty()) {
            int size = mockValidationResultList.size();
            return mockValidationResultList.get(size-1);
        } else {
            return null;
        }
    }
}
