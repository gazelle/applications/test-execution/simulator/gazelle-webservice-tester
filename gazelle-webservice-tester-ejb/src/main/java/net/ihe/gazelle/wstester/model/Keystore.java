package net.ihe.gazelle.wstester.model;

import org.jboss.seam.Component;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "gwt_keystore")
@SequenceGenerator(name = "gwt_keystore_sequence", sequenceName = "gwt_keystore_id_seq", allocationSize = 1)
public class Keystore implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_keystore_sequence")
    private Integer id;

    @Column(name = "label", unique = true, nullable = false)
    private String label;

    @Column(name = "path", unique = true, nullable = false)
    private String path;

    @OneToMany(
            targetEntity = SoapuiProject.class,
            mappedBy = "keystore",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<SoapuiProject> soapuiProjects;

    @Column(name = "password")
    private String password;

    @Column(name = "default_keystore")
    private boolean defaultKeystore;

    //add possibility to duplicate

    public Keystore() {
        super();
    }

    public Keystore(String label, String path) {
        this.label = label;
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<SoapuiProject> getSoapuiProjects() {
        return soapuiProjects;
    }

    public void setSoapuiProjects(List<SoapuiProject> soapuiProjects) {
        this.soapuiProjects = soapuiProjects;
    }

    public void addSoapuiProject(SoapuiProject soapuiProject) {
        if (soapuiProjects == null) {
            soapuiProjects = new ArrayList<>();
        }
        soapuiProjects.add(soapuiProject);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isDefaultKeystore() {
        return defaultKeystore;
    }

    public void setDefaultKeystore(boolean defaultKeystore) {
        this.defaultKeystore = defaultKeystore;
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Keystore keystore = (Keystore) o;

        if (!label.equals(keystore.label)) {
            return false;
        }
        if (!path.equals(keystore.path)) {
            return false;
        }
        if (soapuiProjects != null ? !soapuiProjects.equals(keystore.soapuiProjects) : keystore.soapuiProjects != null) {
            return false;
        }
        return password != null ? password.equals(keystore.password) : keystore.password == null;
    }

    @Override
    public int hashCode() {
        int result = label.hashCode();
        result = 31 * result + path.hashCode();
        result = 31 * result + (soapuiProjects != null ? soapuiProjects.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
