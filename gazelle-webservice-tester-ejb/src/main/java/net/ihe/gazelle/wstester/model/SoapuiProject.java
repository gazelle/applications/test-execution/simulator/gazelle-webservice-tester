package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.preferences.PreferenceService;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "gwt_soapui_project")
@SequenceGenerator(name = "gwt_soapui_project_sequence", sequenceName = "gwt_soapui_project_id_seq", allocationSize = 1)
public class SoapuiProject implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoapuiProject.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_soapui_project_sequence")
    private Integer id;

    @Column(name = "label", unique = true, nullable = false)
    private String label;

    @Column(name = "xml_file_path", nullable = false)
    private String xmlFilePath;

    @Column(name = "version", nullable = false)
    private int version;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "owner", nullable = false)
    private String owner;

    @Column(name = "creation_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "keystore")
    private Keystore keystore;

    @OneToOne(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "gwt_project")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private GwtProject gwtProject;

    public SoapuiProject() {
        super();
    }

    public SoapuiProject(String label, String xmlFilePath, int version, boolean active, String owner, Date creationDate, Keystore keystore) {
        this.label = label;
        this.xmlFilePath = xmlFilePath;
        this.version = version;
        this.active = active;
        this.owner = owner;
        this.creationDate = new Date(creationDate.getTime());
        this.keystore = keystore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getXmlFilePath() {
        return xmlFilePath;
    }

    public void setXmlFilePath(String xmlFilePath) {
        this.xmlFilePath = xmlFilePath;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        if (this.active != active) {
            if (!active) {
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "This project has been deactivated : it won't be executable");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "This project has been activated : it will be executable");
            }
        }
        this.active = active;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getCreationDate() {
        return new Date(creationDate.getTime());
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = new Date(creationDate.getTime());
    }

    public Keystore getKeystore() {
        return keystore;
    }

    public void setKeystore(Keystore keystore) {
        this.keystore = keystore;
    }

    public GwtProject getGwtProject() {
        return gwtProject;
    }

    public void setGwtProject(GwtProject gwtProject) {
        this.gwtProject = gwtProject;
    }

    public String getLink() {
        if (PreferenceService.getString("application_url") != null) {
            return PreferenceService.getString("application_url") + "/project/projectDetail.seam?id=" + this.getId();
        } else {
            LOGGER.error("The preference application_url is not set : can't display project link");
            return null;
        }
    }

    public String edit() {
        return "/project/projectDetail.seam?id=" + id + "&edit=true";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SoapuiProject that = (SoapuiProject) o;

        if (version != that.version) {
            return false;
        }
        if (active != that.active) {
            return false;
        }
        if (!label.equals(that.label)) {
            return false;
        }
        if (!xmlFilePath.equals(that.xmlFilePath)) {
            return false;
        }
        if (!owner.equals(that.owner)) {
            return false;
        }
        if (!creationDate.equals(that.creationDate)) {
            return false;
        }
        if (keystore != null ? !keystore.equals(that.keystore) : that.keystore != null) {
            return false;
        }
        return gwtProject.equals(that.gwtProject);
    }

    @Override
    public int hashCode() {
        int result = label.hashCode();
        result = 31 * result + xmlFilePath.hashCode();
        result = 31 * result + version;
        result = 31 * result + (active ? 1 : 0);
        result = 31 * result + owner.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + (keystore != null ? keystore.hashCode() : 0);
        result = 31 * result + gwtProject.hashCode();
        return result;
    }
}
