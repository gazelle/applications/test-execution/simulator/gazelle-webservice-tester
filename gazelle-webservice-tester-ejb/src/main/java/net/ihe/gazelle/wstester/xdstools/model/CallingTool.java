package net.ihe.gazelle.wstester.xdstools.model;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

/**
 * this class aimed to uniquely identified the tools that can send files to the EVS Client for validation.
 * When the tool send a
 *
 * @author aberge
 */

@Entity
@Name("callingTool")
@Table(name = "gwt_calling_tool")
@SequenceGenerator(name = "gwt_calling_tool_sequence", sequenceName = "gwt_calling_tool_id_seq", allocationSize = 1)
public class CallingTool implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6159285638210857211L;


    @Id
    @GeneratedValue(generator = "gwt_calling_tool_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "label")
    @NotNull
    private String label;

    @Column(name = "oid")
    @NotNull
    //	@Pattern(regex="", message="This attribute must be formatted as a valid OID")
    private String oid;

    @Column(name = "url")
    @NotNull
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$)", message = "This attribute must be formatted as a valid URL")
    private String url;

    @OneToMany(
            targetEntity = TestInstanceTm.class,
            mappedBy = "callingTool",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<TestInstanceTm> testInstanceTmList;

    public CallingTool() {
    }

    public static CallingTool getToolByOid(final String oid) {
        final CallingToolQuery query = new CallingToolQuery();
        query.oid().eq(oid);
        return query.getUniqueResult();
    }

    public static List<CallingTool> getAllTool() {
        final CallingToolQuery query = new CallingToolQuery();
        return query.getListNullIfEmpty();
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getOid() {
        return this.oid;
    }

    public void setOid(final String oid) {
        this.oid = oid;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public List<TestInstanceTm> getTestInstanceTmList() {
        return testInstanceTmList;
    }

    public void setTestInstanceTmList(List<TestInstanceTm> testInstanceTmList) {
        this.testInstanceTmList = testInstanceTmList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CallingTool that = (CallingTool) o;

        if (!label.equals(that.label)) {
            return false;
        }
        if (!oid.equals(that.oid)) {
            return false;
        }
        return url.equals(that.url);
    }

    @Override
    public int hashCode() {
        int result = label.hashCode();
        result = 31 * result + oid.hashCode();
        result = 31 * result + url.hashCode();
        return result;
    }
}
