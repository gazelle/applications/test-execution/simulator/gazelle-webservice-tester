package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.wstester.util.HibernateUtil;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("project")
public class GwtProject extends TestComponent implements Serializable {

    @OneToOne(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "soapui_project")
    private SoapuiProject soapuiProject;

    @OneToMany(
            targetEntity = GwtTestSuite.class,
            mappedBy = "gwtProject",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<GwtTestSuite> testSuites;

    @OneToMany(fetch = FetchType.EAGER,
            targetEntity = GwtProjectResult.class,
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<GwtProjectResult> gwtProjectResults = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<OidProperty> oidPropertyList;


    public GwtProject() {
        super();
    }

    public GwtProject(SoapuiProject soapuiProject, List<GwtTestSuite> testSuites) {
        this.soapuiProject = soapuiProject;
        this.testSuites = testSuites;
    }

    public GwtProject(String label, List<CustomProperty> customProperties, SoapuiProject soapuiProject, List<GwtTestSuite> testSuites, List<OidProperty> oidPropertyList, List<GwtProjectResult> gwtProjectResults) {
        super(label, customProperties);
        this.soapuiProject = soapuiProject;
        this.testSuites = testSuites;
        this.oidPropertyList = oidPropertyList;
        this.gwtProjectResults = gwtProjectResults;
    }

    @Override
    public String getClassName() {
        return GwtProject.class.getSimpleName();
    }

    public SoapuiProject getSoapuiProject() {
        return soapuiProject;
    }

    public void setSoapuiProject(SoapuiProject soapuiProject) {
        this.soapuiProject = soapuiProject;
    }

    public List<GwtTestSuite> getTestSuites() {
        return testSuites;
    }

    public void setTestSuites(List<GwtTestSuite> testSuites) {
        this.testSuites = testSuites;
    }

    public List<GwtProjectResult> getGwtProjectResults() {
        return gwtProjectResults;
    }

    public void setGwtProjectResults(List<GwtProjectResult> gwtProjectResults) {
        if (this.getGwtProjectResults() == null) {
            this.gwtProjectResults = gwtProjectResults;
        }
    }

    public void addGwtProjectResult(List<GwtProjectResult> gwtProjectResults) {
        for (GwtProjectResult projectResult : gwtProjectResults) {
            this.gwtProjectResults.add(projectResult);
        }
    }

    /*public void removeGwtProjectResult(GwtProjectResult projectResult) {
        gwtProjectResults.remove(projectResult);
    }*/

    public List<OidProperty> getOidPropertyList() {
        return oidPropertyList;
    }

    public void setOidPropertyList(List<OidProperty> oidPropertyList) {
        this.oidPropertyList = oidPropertyList;
    }

    public void addOidPropertyToList(List<OidProperty> oidPropertyList) {
        for (OidProperty property : oidPropertyList) {
            if (property != null) {
                this.oidPropertyList.add(property);
            }
        }
    }

    public List<GwtTestSuite> getTestSuitesWithCustomPropertiesToModify(TestComponent testComponent) {
        List<GwtTestSuite> gwtTestSuites = new ArrayList<>();
        testComponent = HibernateUtil.initializeAndUnproxy(testComponent);
        for (GwtTestSuite gwtTestSuite : getTestSuites()) {
            boolean isAlreadyAdded = false;
            if (testComponent.equals(gwtTestSuite) || ((testComponent instanceof GwtTestCase) && ((GwtTestCase) testComponent).getTestSuite()
                    .equals(gwtTestSuite)) || testComponent instanceof GwtProject) {
                for (CustomProperty customProperty : gwtTestSuite.getCustomProperties()) {
                    if (customProperty.isToModify()) {
                        gwtTestSuites.add(gwtTestSuite);
                        isAlreadyAdded = true;
                        break;
                    }
                }
                if (!isAlreadyAdded) {
                    for (GwtTestCase gwtTestCase : gwtTestSuite.getTestCases()) {
                        for (CustomProperty customPropertyTestCase : gwtTestCase.getCustomProperties()) {
                            if (customPropertyTestCase.isToModify()) {
                                gwtTestSuites.add(gwtTestSuite);
                                isAlreadyAdded = true;
                                break;
                            }
                        }
                        if (isAlreadyAdded) {
                            break;
                        }
                    }
                }
            }
        }
        return gwtTestSuites;
    }

    public GwtProject deepCopy() {
        GwtProject gwtProjectCopy = new GwtProject();
        gwtProjectCopy.setLabel(this.getLabel());
        gwtProjectCopy.setExecutable(this.isExecutable());
        copyCustomProperties(this, gwtProjectCopy);
        List<GwtTestSuite> gwtTestSuiteList = new ArrayList<>();
        for (GwtTestSuite gwtTestSuite : this.getTestSuites()) {
            GwtTestSuite gwtTestSuiteCopy = new GwtTestSuite();
            gwtTestSuiteCopy.setLabel(gwtTestSuite.getLabel());
            gwtTestSuiteCopy.setProject(gwtProjectCopy);
            gwtTestSuiteCopy.setExecutable(gwtTestSuite.isExecutable());
            copyCustomProperties(gwtTestSuite, gwtTestSuiteCopy);
            List<GwtTestCase> gwtTestCaseList = new ArrayList<>();
            for (GwtTestCase gwtTestCase : gwtTestSuite.getTestCases()) {
                GwtTestCase gwtTestCaseCopy = new GwtTestCase();
                gwtTestCaseCopy.setLabel(gwtTestCase.getLabel());
                gwtTestCaseCopy.setTestSuite(gwtTestSuiteCopy);
                gwtTestCaseCopy.setExecutable(gwtTestCase.isExecutable());
                copyCustomProperties(gwtTestCase, gwtTestCaseCopy);
                List<GwtTestStep> gwtTestStepList = new ArrayList<>();
                for (GwtTestStep gwtTestStep : gwtTestCase.getTestSteps()) {
                    GwtTestStep gwtTestStepCopy = new GwtTestStep();
                    gwtTestStepCopy.setTestCase(gwtTestCaseCopy);
                    gwtTestStepCopy.setExecutable(gwtTestStep.isExecutable());
                    gwtTestStepCopy.setLabel(gwtTestStep.getLabel());
                    copyCustomProperties(gwtTestStep, gwtTestStepCopy);
                    gwtTestStepList.add(gwtTestStepCopy);
                }
                gwtTestCaseCopy.setTestSteps(gwtTestStepList);
                gwtTestCaseList.add(gwtTestCaseCopy);
            }
            gwtTestSuiteCopy.setTestCases(gwtTestCaseList);
            gwtTestSuiteList.add(gwtTestSuiteCopy);
        }
        gwtProjectCopy.setTestSuites(gwtTestSuiteList);
        return gwtProjectCopy;
    }

    private void copyCustomProperties(TestComponent testComponent, TestComponent testComponentCopy) {
        List<CustomProperty> customPropertiesCopy = new ArrayList<>();
        for (CustomProperty customProperty : testComponent.getCustomProperties()) {
            CustomProperty customPropertyCopy = new CustomProperty();
            customPropertyCopy.setDefaultValue(customProperty.getDefaultValue());
            customPropertyCopy.setLabel(customProperty.getLabel());
            customPropertyCopy.setTestComponent(testComponentCopy);
            customPropertyCopy.setToModify(customProperty.isToModify());
        }
        testComponentCopy.setCustomProperties(customPropertiesCopy);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GwtProject that = (GwtProject) o;

        return testSuites.equals(that.testSuites);
    }

    @Override
    public int hashCode() {
        return testSuites.hashCode();
    }
}
