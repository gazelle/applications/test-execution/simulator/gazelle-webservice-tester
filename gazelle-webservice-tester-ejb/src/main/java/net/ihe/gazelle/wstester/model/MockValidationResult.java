package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.message.model.MessageInstance;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "gwt_mock_validation_result")
@SequenceGenerator(name = "gwt_mock_validation_result_sequence", sequenceName = "gwt_mock_validation_result_id_seq", allocationSize = 1)
public class MockValidationResult implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_mock_validation_result_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "message_instance_id")
    private MessageInstance messageInstance;

    @Column(name = "oid")
    private String oid;

    @Column(name = "validation_date")
    private String validationDate;

    @Column(name = "permanent_link")
    private String permanentLink;

    public MockValidationResult() {
        super();
    }

    public MockValidationResult(MessageInstance messageInstance, String oid) {
        this.messageInstance = messageInstance;
        this.oid = oid;
    }

    public MockValidationResult save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        MockValidationResult result = entityManager.merge(this);
        entityManager.flush();
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MessageInstance getMessageInstance() {
        return messageInstance;
    }

    public void setMessageInstance(MessageInstance messageInstance) {
        this.messageInstance = messageInstance;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    public String getPermanentLink() {
        return permanentLink;
    }

    public void setPermanentLink(String permanentLink) {
        this.permanentLink = permanentLink;
    }

}
