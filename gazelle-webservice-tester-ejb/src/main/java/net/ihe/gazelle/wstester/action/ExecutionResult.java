package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.wstester.dao.ExecutionDao;
import net.ihe.gazelle.wstester.dao.GwtProjectResultDao;
import net.ihe.gazelle.wstester.model.CustomPropertyUsed;
import net.ihe.gazelle.wstester.model.Execution;
import net.ihe.gazelle.wstester.model.TestComponent;
import net.ihe.gazelle.wstester.util.Application;
import net.ihe.gazelle.wstester.util.Tree;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.security.Identity;
import org.richfaces.event.ItemChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


@Name("executionResult")
@Scope(ScopeType.PAGE)
public class  ExecutionResult implements UserAttributeCommon {

    private static final Logger LOG = LoggerFactory.getLogger(ExecutionResult.class);
    private Execution execution;
    private List<CustomPropertyUsed> customPropertyUsedList;
    private GazelleTreeNodeImpl<TestComponent> tree;

    @RequestParameter("id")
    private Integer id;

    @RequestParameter("validation")
    private String current;

    @In
    private EntityManager entityManager;

    @In(value="gumUserService")
    private UserService userService;

    private boolean singleMode;

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public List<CustomPropertyUsed> getCustomPropertyUsedList() {
        return customPropertyUsedList;
    }

    public void setCustomPropertyUsedList(List<CustomPropertyUsed> customPropertyUsedList) {
        this.customPropertyUsedList = customPropertyUsedList;
    }

    public void setCustomPropertyUsedList(TestComponent testComponent) {
        this.customPropertyUsedList = testComponent.getCustomPropertiesUsed(execution);
    }


    /**
     * Called in executionResult.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GazelleTreeNodeImpl<TestComponent> getTree() {
        return tree;
    }

    public void setTree(GazelleTreeNodeImpl<TestComponent> tree) {
        this.tree = tree;
    }

    @Create
    public void init() {

        execution = ExecutionDao.getExecutionById(id);
        if (execution.getTestComponent() != null) {
            tree = Tree.getComponentWithParentsAsTree(execution.getTestComponent());
        } else if (execution.getTestInstanceResult() != null && execution.getTestInstanceResult().getTestSuite() != null) {
            tree = Tree.getComponentWithParentsAsTree(execution.getTestInstanceResult().getTestSuite());
        }

    }

    public String getSoapuiProject() {
        if (execution.getTestStepResults() != null) {
            return execution.getTestStepResults().get(0).getTestStep().getTestCase().getTestSuite().getProject().getLabel();
        }
        ExecutionResult.LOG.error("The execution doesn't have any result");
        return "null";
    }

    public String getSoapuiLog() {
        try {
            return new String(Files.readAllBytes(Paths.get(PreferenceService.getString("logs_directory") + this.execution.getId() + "/soapui.log"))
                    , StandardCharsets.UTF_8);
        } catch (IOException e) {
            //LOG.error(e.getMessage());
        }
        return null;
    }

    public String getSoapuiErrorLog() {
        try {
            return new String(Files.readAllBytes(Paths.get(PreferenceService.getString("logs_directory") + this.execution.getId() + "/soapui-errors" +
                    ".log")), StandardCharsets.UTF_8);
        } catch (IOException e) {
            //LOG.error(e.getMessage());
        }
        return null;
    }

    public String getGroovyLog() {
        try {
            return new String(Files.readAllBytes(Paths.get(PreferenceService.getString("logs_directory") + this.execution.getId() + "/global-groovy" +
                    ".log")), StandardCharsets.UTF_8);
        } catch (IOException e) {
            //LOG.error(e.getMessage());
        }
        return null;
    }

    public String getLink() {
        if (PreferenceService.getString("application_url") != null) {

            return PreferenceService.getString("application_url") + "/executionResult.seam?id=" + execution.getId();
        } else {
            LOG.error("The preference application_url is not set : can't display instance result link");
            return null;
        }
    }

    public boolean isUserAllowed() {
        if (Identity.instance().hasRole("admin_role") ||
                Identity.instance().hasRole("monitor_role") ||
                Application.getCompanyKeyword().equals(execution.getCompany())) {
            return true;
        }
        return false;
    }

    public boolean isCustomPropertyUsedListEmpty() {
        if (customPropertyUsedList != null && customPropertyUsedList.isEmpty()) {
            return true;
        }
        return false;
    }

    public int numberOfCustumPropertiesUsed(TestComponent testComponent) {
        return testComponent.getCustomPropertiesUsed(execution).size();
    }

    public String isCrossValidationStatusAvailable(String oid) {
        String status = GwtProjectResultDao.getXValidatorStatusFromOid(oid);
        if (status != null && !status.isEmpty()) {
            if (status.equals("Available")) {
                return "Available";
            } else {
                return "Not available";
            }
        } else {
            return "Not existing";
        }
    }

    public boolean isSingleMode() {
        return singleMode;
    }

    public void setSingleMode(boolean singleMode) {
        this.singleMode = singleMode;
    }

    public String getCurrent() {
        return this.current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public void updateCurrent(ItemChangeEvent event) {
        setCurrent(event.getNewItemName());
    }

    public String reRun(Execution execution) {
        RunInstanceAction runner = new RunInstanceAction();
        execution = entityManager.find(Execution.class, execution.getId());
        runner.reRunTestInstance(execution);
        return "/executionResult.seam?id=" + runner.getExecution().getId();
    }
}
